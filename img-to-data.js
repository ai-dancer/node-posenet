#!/usr/bin/env node 

process.env['TF_CPP_MIN_LOG_LEVEL'] = '2'

require('@tensorflow/tfjs-node');
const posenet = require('@tensorflow-models/posenet');
const pixelUtil = require('pixel-util');
const { Canvas, Image } = require('canvas');
const mitm = require('mitm')();
const fs = require('fs');

const fast = false;
let model = null;

mitm.on('request', (req,res) => {
    const uri = 'https://'+req.headers.host + req.url; 
    // console.error(uri);
    let file = uri.split('/');
    file = file[file.length-1];
    const data = fs.readFileSync(`./models/${file}`)
    res.end(data);
})

// Fast architecture
const MobileNetV1 =   {
    architecture: 'MobileNetV1',
    outputStride: 16,
    multiplier: 0.75
};

// Better, slower
const ResNet50 = {
    architecture: 'ResNet50',
    outputStride: 32,
    quantBytes: 2
    // inputResolution: { width, height},
};

const arch = fast?MobileNetV1:ResNet50;

posenet.load(arch).then(net => {
    model = net;
    process.stdin.resume();
});

let processedFrames = 0;

const makeEstimation = async imageData => {
    process.stdin.pause();
    return pixelUtil.createBuffer(imageData).then(function (buffer) {
        try {
            const image = new Image;
            image.src = buffer;

            const canvas = new Canvas(image.width, image.height);
            const ctx = canvas.getContext('2d');

            ctx.drawImage(image, 0, 0);

            return model.estimateSinglePose(canvas, {
                flipHorizontal: false
            }).then(pose => {
                // const labels = ['frame', 'width', 'height','score', ...pose.keypoints.map(kp => {
                //     return [`${kp.part}.x`, `${kp.part}.y`];
                // })].flat();

                const line = [processedFrames++, image.width, image.height, pose.score, ...pose.keypoints.map(kp => {
                    // return [kp.position.x, kp.position.y];
                    return [kp.position.y, kp.position.x];
                })].flat();

                process.stdin.resume();
                return line;
            });
        }
        catch (err) {
            console.error(err);
        }
    });

};

let data = Buffer.from('');

const addData = chunk => {

    if(chunk) data = Buffer.concat([data,Buffer.from(chunk)]);
    // console.log('1:', data.subarray(0,4), data.subarray(data.length-4));

    let soi =  data.indexOf("FFD8", 0, "hex");
    let eoi = data.indexOf("FFD9", 0, "hex");
    
    while (soi!==-1 && eoi!==-1) {
        const imageData = data.subarray(soi, eoi + 2);
        // console.log('2:',imageData.subarray(0,4), imageData.subarray(imageData.length-4));
        makeEstimation(imageData).then(pose => {
            console.info(JSON.stringify(pose));
        });
        data = data.subarray(eoi+2);
        soi =  data.indexOf("FFD8", 0, "hex");
        eoi = data.indexOf("FFD9", 0, "hex");    
    } 
};

process.stdin
    .on('data', addData)
    .on('end', addData)
    .on('error',console.error)
    .pause();

