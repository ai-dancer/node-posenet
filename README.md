# node-posenet

## Usage: 

# Full cycle (create skeleton video)

## video to video
```
ffmpeg -i ../sample-video/sample.mp4 -an -r 25  -hide_banner -loglevel quiet -vcodec mjpeg -f image2pipe - | \
./img-to-data.js | \
./data-to-img.js | \
ffmpeg -y -f image2pipe -i - -hide_banner -loglevel quiet -c:v libx264 -vf format=yuv420p -r 25 out.mp4
```

```
ffmpeg -i ../sample-video/sample.mp4 -an -r 25  -hide_banner -loglevel quiet -vcodec mjpeg -f image2pipe - | \
./img-to-data.js | \
./data-to-csv.js > keypoints.csv
```

To limit number of frames, add this param:
`-vframes 30`


```
const labels = [
    'frame',
'score',
'nose.x',           'nose.y',
'leftEye.x',        'leftEye.y',
'rightEye.x',       'rightEye.y',
'leftEar.x',        'leftEar.y',
'rightEar.x',       'rightEar.y',
'leftShoulder.x',   'leftShoulder.y',
'rightShoulder.x',  'rightShoulder.y',
'leftElbow.x',      'leftElbow.y',
'rightElbow.x',     'rightElbow.y',
'leftWrist.x',      'leftWrist.y',
'rightWrist.x',     'rightWrist.y',
'leftHip.x',        'leftHip.y',
'rightHip.x',       'rightHip.y',
'leftKnee.x',       'leftKnee.y',
'rightKnee.x',      'rightKnee.y',
'leftAnkle.x',      'leftAnkle.y',
'rightAnkle.x',     'rightAnkle.y'
];              
```




/////  AUDIO FEATURES

I use an unorthodox audio sample rate of 25.600Hz so I can get a pow(2) number of samples per video frame (as required for the audio feature extraction library)

26.500 frames per second give 1.024 samples every 40 ms (1 video frame at 25 FPS)

[zcr, spectralFlatness, ...mfcc]

find more info at https://meyda.js.org/audio-features

```
ffmpeg -y -i ../sample-video/sample.mp4 -hide_banner -loglevel quiet -vn -ar 25600 -ac 1 -f s16le -c:a pcm_s16le - | ./pcm-to-features.js | ./data-to-csv.js > audio-features.csv
```