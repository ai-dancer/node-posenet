#!/usr/bin/env node

let data = Buffer.from('');

// const keypoointsHeader = [
//     'frame',
//     'score',
//     'nose.y', 'nose.x',
//     'leftEye.y', 'leftEye.x',
//     'rightEye.y', 'rightEye.x',
//     'leftEar.y', 'leftEar.x',
//     'rightEar.y', 'rightEar.x',
//     'leftShoulder.y', 'leftShoulder.x',
//     'rightShoulder.y', 'rightShoulder.x',
//     'leftElbow.y', 'leftElbow.x',
//     'rightElbow.y', 'rightElbow.x',
//     'leftWrist.y', 'leftWrist.x',
//     'rightWrist.y', 'rightWrist.x',
//     'leftHip.y', 'leftHip.x',
//     'rightHip.y', 'rightHip.x',
//     'leftKnee.y', 'leftKnee.x',
//     'rightKnee.y', 'rightKnee.x',
//     'leftAnkle.y', 'leftAnkle.x',
//     'rightAnkle.y', 'rightAnkle.x'
// ];


// const  mfccHeader = [
//     'zcr',
//     'mfcc01',
//     'mfcc02',
//     'mfcc03',
//     'mfcc04',
//     'mfcc05',
//     'mfcc06',
//     'mfcc07',
//     'mfcc08',
//     'mfcc09',
//     'mfcc10',
//     'mfcc11',
//     'mfcc12',
//     'mfcc13',
// ];

const addData = chunk => {
    if (chunk) data = Buffer.concat([data, Buffer.from(chunk)]);

    let soi = data.indexOf("[");
    let eoi = data.indexOf("]");

    while (soi !== -1 && eoi !== -1) {
        const frameData = data.subarray(soi + 1, eoi);
        console.log(frameData.toString());
        data = data.subarray(eoi + 1);
        soi = data.indexOf("[");
        eoi = data.indexOf("]");
    }
};

process.stdin
    .on('data', addData)
    .on('end', addData)
    .on('error', console.error);
