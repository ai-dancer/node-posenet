#!/usr/bin/env sh

dir=$1

echo "processing files in $dir"

mkdir -p ./data/keypoints
mkdir -p ./data/audio

for f in $dir/*.mp4; do 
    name=`basename $f`

    FILE=./data/keypoints/$name.csv
    if [ -f "$FILE" ]; then
        echo "$FILE exist"
    else 

    
    echo "Processing Audio Features for $name"
    ffmpeg -y -i $f -hide_banner -loglevel quiet -vn -ar 25600 -ac 1 -f s16le -c:a pcm_s16le - | \
    ./pcm-to-features.js |\
     ./data-to-csv.js > ./data/audio/$name-tmp.csv

    linesFeatures=`wc -l ./data/audio/$name-tmp.csv |  awk '{print $1}'`
    echo "$linesFeatures lines"

    echo "Processing PoseNet KeyPoints for $name"
    ffmpeg -i $f -an -r 25  -hide_banner -loglevel quiet -vcodec mjpeg -f image2pipe - | \
    ./img-to-data.js | \
    ./data-to-csv.js > ./data/keypoints/$name-tmp.csv

    linesKeypoints=`wc -l ./data/keypoints/$name-tmp.csv |  awk '{print $1}'`
    echo "$linesKeypoints lines"

    lines=`node -e "console.log(Math.min($linesKeypoints,$linesFeatures))"`

    cat ./data/keypoints/$name-tmp.csv | head -$lines > ./data/keypoints/$name.csv
    cat ./data/audio/$name-tmp.csv | head -$lines > ./data/audio/$name.csv

    rm ./data/keypoints/$name-tmp.csv
    rm ./data/audio/$name-tmp.csv   
    fi

done



