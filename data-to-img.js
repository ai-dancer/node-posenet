#!/usr/bin/env node

const { Canvas } = require('canvas');

let data = Buffer.from('');


const makeImage = async (frameData) => {
    try {
        const data = JSON.parse(frameData.toString());
        const canvas = new Canvas(data[1], data[2]);
        const ctx = canvas.getContext('2d');

        ctx.strokeStyle = "#FF0000";

        ctx.beginPath();

        for (let i = 3; i < data.length; i += 2) {
            ctx.moveTo(data[i], data[i + 1]);
            ctx.lineTo(data[i] + 1, data[i + 1] + 1);
            ctx.stroke();
        }

        ctx.moveTo(data[21], data[22]);
        ctx.lineTo(data[17], data[18]);
        ctx.lineTo(data[13], data[14]);
        ctx.lineTo(data[15], data[16]);
        ctx.lineTo(data[19], data[20]);
        ctx.lineTo(data[23], data[24]);

        ctx.moveTo(data[13], data[14]);
        ctx.lineTo(data[25], data[26]);
        ctx.lineTo(data[27], data[28]);
        ctx.lineTo(data[15], data[16]);

        ctx.moveTo(data[25], data[26]);
        ctx.lineTo(data[29], data[30]);
        ctx.lineTo(data[33], data[34]);

        ctx.moveTo(data[27], data[28]);
        ctx.lineTo(data[31], data[32]);
        ctx.lineTo(data[35], data[36]);
        ctx.stroke();

        /**
         * 
         3   'nose.x',           'nose.y',
         5   'leftEye.x',        'leftEye.y',
         7   'rightEye.x',       'rightEye.y',
         9   'leftEar.x',        'leftEar.y',
         11   'rightEar.x',       'rightEar.y',
         13   'leftShoulder.x',   'leftShoulder.y',
         15   'rightShoulder.x',  'rightShoulder.y',
         17   'leftElbow.x',      'leftElbow.y',
         19   'rightElbow.x',     'rightElbow.y',
         21   'leftWrist.x',      'leftWrist.y',
         23   'rightWrist.x',     'rightWrist.y',
         25   'leftHip.x',        'leftHip.y',
         27   'rightHip.x',       'rightHip.y',
         29   'leftKnee.x',       'leftKnee.y',
         31   'rightKnee.x',      'rightKnee.y',
         33   'leftAnkle.x',      'leftAnkle.y',
         35   'rightAnkle.x',     'rightAnkle.y'
         */

        // const img = canvas.toDataURL().replace(/^data:image\/\w+;base64,/, "");
        // const buf = new Buffer(img, 'base64');
        const img = canvas.toBuffer();
        return Promise.resolve(img);
    }
    catch (err) {
        console.error(err);
    }
};

const addData = chunk => {
    if (chunk) data = Buffer.concat([data, Buffer.from(chunk)]);

    let soi = data.indexOf("[");
    let eoi = data.indexOf("]");

    while (soi !== -1 && eoi !== -1) {
        const frameData = data.subarray(soi, eoi+1);
        makeImage(frameData).then(img => process.stdout.write(img));
        data = data.subarray(eoi+1);
        soi = data.indexOf("[");
        eoi = data.indexOf("]");
    }
};

process.stdin
    .on('data', addData)
    .on('end', addData)
    .on('error', console.error);
