#!/usr/bin/env node

/**
Usage: 
ffmpeg -y -i ../sample-video/sample.mp4 -vn -ar 25600 -ac 1 -f s16le -c:a pcm_s16le | ./pcm-to-features.js

The script returns a line of 1024 samples every 40ms
(assuming a sample rate of 25600Hz)

Video frame rate = 25 FPS --> 1 frame every 40 ms
*/

const Meyda = require('meyda');

let samples = [];

const audioSamplesPerVideoFrame = 1024; // 25600/25;

const getFeatures = signal => {
  const mfcc = Meyda.extract('mfcc', signal);
  const zcr = Meyda.extract('zcr', signal);
  const spectralFlatness = Meyda.extract('spectralFlatness', signal);
  return [zcr, spectralFlatness, ...mfcc.map(c => c ? c : 0)];
};

const decodeBuffer = buffer => {
  if (!buffer) return;
  const data = Array.from(
    { length: buffer.length / 2 },
    (v, i) => buffer.readInt16LE(i * 2) / (2 ** 15)
  );
  samples = [...samples, ...data];
  while (samples.length > audioSamplesPerVideoFrame) {
    const signal = samples.splice(0, audioSamplesPerVideoFrame);
    const features = getFeatures(signal);
    console.log(JSON.stringify(features));
  }
}

process.stdin
  .on('data', decodeBuffer)
  .on('end', decodeBuffer)
  .on('error', console.error);
